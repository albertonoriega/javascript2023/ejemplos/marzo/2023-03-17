// introducimos un texto y nos dice el numero de vocales que tiene
let vocales = ['a', 'e', 'i', 'o', 'u'];

let boton = document.querySelector('button');


boton.addEventListener("click", function () {
    let cont = 0;
    let texto = document.querySelector('#texto').value;

    for (let i = 0; i < texto.length; i++) {
        for (let j = 0; j < vocales.length; j++) {
            if (texto[i].toLowerCase() == vocales[j]) {
                cont++;
                break;
            }
        }
    }
    document.querySelector('.salida').innerHTML =
        "Longitud del texto: " + texto.length + "<br>" +
        "Número de vocales: " + cont;
});



