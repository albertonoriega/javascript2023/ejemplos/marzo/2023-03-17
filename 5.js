// Pedir un numero por teclado ( máximo 9)
// Mostrar como imagenes los numerps desde 0 hasta el numero introducido

let numero = prompt("Introduce un número menor que 10");

const fotos = [
    '1.jpg',
    '2.jpg',
    '3.jpg',
    '4.jpg',
    '5.jpg',
    '6.jpg',
    '7.jpg',
    '8.jpg',
    '9.jpg',
];

// Para evitar errores si el numero es mayor que nueves
if (numero > 9) {
    numero = 9;
}

for (let i = 1; i <= numero; i++) {
    document.write(`<img src="imgs\\${fotos[i - 1]}">`);



}