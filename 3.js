// Torre de numeros

let numero = +prompt("Introduce número de la torre");

let resultado = torreNumeros(numero);

document.write(resultado);


function torreNumeros(numeroTorre) {

    let salida = "";
    let contador = "";
    for (let i = 1; i <= numeroTorre; i++) {
        // contador por vuelta
        contador = contador.concat(i);
        //contador total con el resultado
        salida += contador + "<br>";
    }
    return salida;
}